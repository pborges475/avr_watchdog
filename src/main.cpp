#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <util/delay.h>

#define SLEEP_CYCLES 3

void blink(uint8_t count);

void wakeup();
void work();
void sleep();





volatile int watchdog_counter;
ISR(WDT_vect) 
{
  	watchdog_counter++;
}
// 0=16ms, 1=32ms, 2=64ms, 3=128ms, 4=250ms, 5=500ms
// 6=1sec, 7=2sec, 8=4sec, 9=8sec
void setup_watchdog(int timerPrescaler) 
{
	cli();
	if (timerPrescaler > 9 ) timerPrescaler = 9; //Correct incoming amount if need be

	uint8_t bb = timerPrescaler & 7; 
	if (timerPrescaler > 7) bb |= (1<<5); //Set the special 5th bit if necessary

	//This order of commands is important and cannot be combined
	MCUSR &= ~(1<<WDRF); //Clear the watch dog reset
	WDTCSR |= (1<<WDCE) | (1<<WDE); //Set WD_change enable, set WD enable
	WDTCSR = bb; //Set new watchdog timeout value
	WDTCSR |= _BV(WDIE); //Set the interrupt enable, this will keep unit from resetting after each int
	sei();
}
void setup_sleep()
{
	set_sleep_mode(SLEEP_MODE_PWR_DOWN); 
	sleep_enable();	
}
int main()
{
	setup_sleep();
	setup_watchdog(6);

	wakeup();	
	work();
	
	while(1)
	{
		if(watchdog_counter > 10)
		{
			watchdog_counter = 0;
			wakeup();
			work();
		}
		sleep();
	}
}
void work()
{
	blink(1);
}
void sleep()
{
	DDRB &= ~(1<<PB1);
	ADCSRA &= ~(1<<ADEN);
  	sleep_mode();
}
void wakeup()
{
	DDRB |= (1<<PB1);
	ADCSRA |= (1<<ADEN);
}
void blink(uint8_t count)
{
	for(int i=0;i<count;i++)
	{
		PORTB |= (1<<PB1);
		_delay_ms(500);
		PORTB &= ~(1<<PB1);
		_delay_ms(250);
	}
}